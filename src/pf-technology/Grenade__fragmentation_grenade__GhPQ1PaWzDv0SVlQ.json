{
  "_id": "GhPQ1PaWzDv0SVlQ",
  "name": "Grenade (fragmentation grenade)",
  "type": "consumable",
  "img": "icons/weapons/thrown/bomb-fuse-black-pink.webp",
  "flags": {
    "autoanimations": {
      "killAnim": false,
      "override": false,
      "animType": "t1",
      "animName": "",
      "bards": {
        "bardAnim": "a1",
        "bardTarget": true,
        "bardTargetAnim": "a1",
        "bardTargetColor": "a1",
        "bardSelf": true,
        "marker": true,
        "markerColor": "a1",
        "markerColorTarget": "a1"
      },
      "spellVar": "a1",
      "hmAnim": "a1",
      "color": "n1",
      "spellOptions": {
        "spellLoops": 1
      },
      "uaStrikeType": "physical",
      "dtvar": "dt1",
      "divineSmite": {
        "dsEnable": false,
        "dsSelf": true,
        "dsSelfDelay": 0,
        "dsSelfColor": "a1",
        "dsTarget": true,
        "dsTargetDelay": 1250,
        "dsTargetColor": "a1"
      },
      "explosion": false,
      "explodeVariant": "ev1",
      "explodeColor": "",
      "explodeRadius": "0",
      "explodeLoop": "1",
      "allSounds": {
        "explosion": {
          "audioExplodeEnabled": false,
          "file": "",
          "delay": 100,
          "volume": 25
        },
        "item": {
          "enableAudio": false,
          "file": "",
          "delay": 100,
          "volume": 25
        }
      },
      "selfRadius": "5",
      "animTint": "#ffffff",
      "auraOpacity": 0.75,
      "ctaOption": false,
      "teleDist": 30
    }
  },
  "system": {
    "description": {
      "value": "<p><strong>Source</strong> <em>Technology Guide pg. 46</em></p><h2>Statistics</h2></h2><p><strong>Slot</strong> none</p><p><strong>Capacity</strong> -; <strong>Usage</strong> disposable</p><h2>Description</h2><p>A grenade is a small, cylindrical device that is designed to be thrown as a splash weapon or fired from a grenade launcher (see page 24). Before being thrown by hand, the grenade must be primed with a quick twist of a dial at one end and then armed with a click of a button at the center of that dial. Priming and arming a grenade is a free action; a grenade launcher primes and arms all grenades it fires. The grenade detonates at the beginning of the wielder's next turn, hopefully in the area targeted. When a grenade detonates, it damages all targets within a 20-footradius spread. A successful DC 15 Reflex save halves any damage dealt by a grenade.</p><p>The type of damage dealt by grenades varies widely and depends upon the nature of the specific grenade. The different types of grenades and the damage types they deal (along with any additional effects their damage deals) are listed below.</p><ul><li><strong>Arc Grenade</strong>: Deals 5d6 points of electricity damage.</li><li><strong>Atom Grenade</strong>: Creates a 20-foot-radius area of medium radiation that persists for 24 hours.</li><li><strong>Bang Grenade</strong>: Deals no damage but staggers creatures for 1 round and deafens them for 1d4 rounds (DC 15 Fortitude save negates).</li><li><strong>Bio Grenade</strong>: Infects creatures who fail a DC 15 Fortitude save with bubonic plague (no onset).</li><li><strong>Concussion Grenade</strong>: Deals 5d6 points of bludgeoning damage.</li><li><strong>EMP Grenade</strong>: Deals 5d6 points of electricity damage to robots and electronic-based gear, half damage to cyborgs and androids, and no damage to other creatures.</li><li><strong>Flash Grenade</strong>: Blinds creatures for 1d4 rounds (DC 15 Fortitude save negates).</li><li><strong>Flechette Grenade</strong>: Deals 5d6 points of piercing damage.</li><li><strong>Fragmentation Grenade</strong>: Deals 5d6 points of slashing damage.</li><li><strong>Gravity Grenade</strong>: Deals 5d6 points of force damage. Creatures damaged by a gravity grenade are automatically subjected to a trip attack (grenade's CMB = +15).</li><li><strong>Inferno Grenade</strong>: Deals 5d6 points of fire damage.</li><li><strong>Plasma Grenade</strong>: Deals 4d6 points of fire damage and 4d6 points of electricity damage.</li><li><strong>Soft Grenade</strong>: Deals 5d6 points of nonlethal damage.</li><li><strong>Sonic Grenade</strong>: Deals 5d6 points of sonic damage; creatures who fail a DC 15 Fortitude save are also deafened for 1d4 rounds.</li><li><strong>Zero Grenade</strong>: Deals 5d6 points of cold damage.</li></ul><h2>Construction</h2><p><strong>Craft</strong> DC 20 (fragmentation grenade); <strong>Cost</strong> 375 gp (fragmentation grenade)</p><p>Craft Technological Arms and Armor, military lab</p>",
      "unidentified": ""
    },
    "tags": [],
    "quantity": 1,
    "weight": {
      "value": 1
    },
    "price": 750,
    "size": "med",
    "equipped": true,
    "resizing": false,
    "identified": true,
    "hp": {
      "max": 10,
      "value": 10
    },
    "broken": false,
    "hardness": 0,
    "carried": true,
    "unidentified": {
      "price": 0,
      "name": ""
    },
    "cl": 0,
    "aura": {
      "custom": false,
      "school": ""
    },
    "actions": [
      {
        "_id": "johp3gogfr347355",
        "name": "Use",
        "img": "icons/weapons/thrown/bomb-fuse-black-pink.webp",
        "description": "",
        "tag": "grenadeFragmentationGrenade",
        "activation": {
          "cost": 1,
          "type": "standard",
          "unchained": {
            "cost": 2,
            "type": "action"
          }
        },
        "duration": {
          "value": null,
          "units": ""
        },
        "target": {
          "value": ""
        },
        "range": {
          "units": "",
          "maxIncrements": 1,
          "minValue": null,
          "minUnits": ""
        },
        "uses": {
          "autoDeductChargesCost": "",
          "self": {
            "value": 0,
            "maxFormula": "",
            "per": null
          },
          "per": "",
          "value": 0,
          "maxFormula": "",
          "pricePerUse": 0,
          "max": 0
        },
        "measureTemplate": {
          "type": "circle",
          "size": "20",
          "overrideColor": false,
          "customColor": "",
          "overrideTexture": false,
          "customTexture": ""
        },
        "attackName": "",
        "actionType": "other",
        "attackBonus": "",
        "critConfirmBonus": "",
        "damage": {
          "parts": [
            {
              "formula": "5d6",
              "type": {
                "values": [
                  "slashing"
                ],
                "custom": ""
              }
            }
          ],
          "critParts": [],
          "nonCritParts": []
        },
        "attackParts": [],
        "formulaicAttacks": {
          "count": {
            "formula": "",
            "value": null
          },
          "bonus": {
            "formula": ""
          },
          "label": ""
        },
        "formula": "",
        "ability": {
          "attack": "",
          "damage": "",
          "damageMult": 1,
          "critRange": 20,
          "critMult": 2
        },
        "save": {
          "dc": "15",
          "type": "ref",
          "description": "Reflex half"
        },
        "effectNotes": [],
        "attackNotes": [],
        "soundEffect": "",
        "powerAttack": {
          "multiplier": "",
          "damageBonus": 2,
          "critMultiplier": 1
        },
        "naturalAttack": {
          "primaryAttack": true,
          "secondary": {
            "attackBonus": "-5",
            "damageMult": 0.5
          }
        },
        "nonlethal": false,
        "touch": false,
        "usesAmmo": false,
        "spellEffect": "",
        "spellArea": "",
        "conditionals": [],
        "enh": {
          "value": null
        }
      }
    ],
    "uses": {
      "per": "",
      "value": 0,
      "maxFormula": "",
      "pricePerUse": 0,
      "autoDeductChargesCost": "1",
      "max": 0
    },
    "attackNotes": [],
    "effectNotes": [],
    "links": {
      "children": [],
      "charges": []
    },
    "tag": "grenadeFragmentationGrenade",
    "useCustomTag": false,
    "flags": {
      "boolean": {},
      "dictionary": {}
    },
    "scriptCalls": [],
    "subType": "misc",
    "showInQuickbar": false,
    "changes": [],
    "changeFlags": {
      "loseDexToAC": false,
      "noEncumbrance": false,
      "mediumArmorFullSpeed": false,
      "heavyArmorFullSpeed": false
    },
    "contextNotes": []
  },
  "ownership": {
    "default": 0
  },
  "_stats": {
    "systemId": "pf1",
    "systemVersion": "9.2",
    "coreVersion": "11.306",
    "createdTime": 1665428387690,
    "modifiedTime": 1690282446551,
    "lastModifiedBy": "PIulO4wVa7BrTZWn"
  },
  "effects": [],
  "folder": "qNFK7JR5XyV7kUIw",
  "sort": 150098,
  "_key": "!items!GhPQ1PaWzDv0SVlQ"
}