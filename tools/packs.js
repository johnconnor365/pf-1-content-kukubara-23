import { exec } from "child_process";
import { readdirSync, statSync } from "fs";
import { join } from "path";
import url from "node:url";
import yargs from "yargs";

const __filename = url.fileURLToPath(import.meta.url);
const getDirectories = async function(srcPath) { return readdirSync(srcPath).filter(file => statSync(join(srcPath, file)).isDirectory()) };

const Execute = async function(command) { exec(command, (error, stdout, stderr) => {
    if (error) {
        console.log(`error: ${error.message}`);
        return;
    }
    if (stderr) {
        console.log(`stderr: ${stderr}`);
        return;
    }
    console.log(`stdout: ${stdout}`);
})};

// Only handle commands if this script was executed directly
console.log("argv is ", process.argv);
if (process.argv[1] === __filename || process.argv[2] === "pack") {
    yargs(process.argv.slice(2))
      .demandCommand(1, 1)
      .command({
        command: "pack",
        describe: `Compile all packs (or a pack) into ldb file(s)`,
        handler: async (argv) => {
          await compilePacks(argv.folder ?? process.argv[3] ?? null);
        },
      })
      .command({
        command: "unpack",
        describe: `Extract all packs (or a pack) into source JSONs`,
        handler: async (argv) => {
          await extractPacks(argv.folder ?? process.argv[3] ?? null);
        },
      })
      // Option to overwrite the default `reset` option
      .option("folder", { describe: "Work with a specific pack", type: "string" })
      .parse();
  }

async function compilePacks(pack) {
    await Execute(`fvtt package workon "pf-content"`);
    let folders = [];
    if(pack) {
        folders.push(pack);
    }
    else {
        await getDirectories("src").then(async function(results) {
            results.forEach(element => { folders.push(element); });
        });
    }

    console.log(folders);
    folders.forEach(async nextPack => {
        const command = `fvtt package pack -n "${nextPack}" --id "src/${nextPack}" --od "release/packs"`
        console.log(`Packing Compendium ${nextPack} from source in src/${nextPack}:\n`)
        await Execute(command);
    })
}

async function extractPacks(pack) {
    await Execute(`fvtt package workon "pf-content"`);
    let folders = [];
    if(pack) {
        folders.push(pack);
    }
    else {
        await getDirectories("src").then(async function(results) {
            results.forEach(element => { folders.push(element); });
        });
    }

    console.log(folders);
    folders.forEach(async nextPack => {
        const command = `fvtt package unpack -n "${nextPack}" --od "src/${nextPack}"`
        console.log(`Unpacking Compendium ${nextPack} into src/${nextPack}:\n`)
        await Execute(command);
    })
}